# Использование if конструкции
1. Здесь писать конструкцию else не имеет никакого смысла:
```
function isConvert(value, callback){
	if(callback(value) + value < 0){
		return true;
	}else{
		return false;
	}
}
```
2. Лучше написать так:
```
function isConvert(value, callback){
	if(callback(value) + value < 0){
		return true;
	}
	
	return false;
}
```
2. Писать условие, тоже лишено всякого смысла, оно в результате выполнения получает результат true/false
```
function isConvert(value, callback){
	return callback(value) + value < 0;
}
```
3. Для более приятного просмотра кода, можно сделать вот так:
```
function isConvert(value, callback){
	return (callback(value) + value) < 0;
}
```
# Старайтесь не использовать конструкции switch, if, if-elseif-else
1. Не используйте switch, пример:
```
function export(products, type){
    let result = null;

    switch(type){
        case "xml": result = productXmlService.export(products); break;
        case "yml": result = productYmlService.export(products); break;
        case "csv": result = productCsvService.export(products); break;
        case "xslx": result = productXlsxService.export(products); break;
        default: throw new ProductExportError("export with type: '" + type + "' not found");
    }

    return result;
}
```
2. Не используйте if-elseif-else, пример:
```
function export(products, type){
    let result = null; 
    
    if(type === "xml") {
        result = productXmlService.export(products);
    } else if(type === "yml") {
        result = productYmlService.export(products);
    } else if(type === "csv") {
        result = productCsvService.export(products);
    } else if(type === "xlsx") {
        result = productXlsxService.export(products);
    } else {
        throw new ProductExportError("export with type: '" + type + "' not found");
    }

    return result;
}
```
# Старайтесь не использовать тернарные операторы
1. Использование тернарного оператора допустимо в крайне редких ситуациях
```
    function report(done){
        let value = typeof done === "function" ? done() : 0;

        // Далее код        
    }
```
2. Не используйте никогда тернарный оператор внутри тернарного оператора
```
    function report(done){
            let value = typeof done === "function" ? done() : done > 0 ? done : -1;
    
            // Далее код        
        }
```
3. Конструкция if всегда будет гораздо лучше читаема, нежели тернарные операторы
# 