# Правила именования функций
1. Функция всегда должна называться глоголом. Глагол - это действие которое выполняет функция.
    * move
    * draw
    * clear
    * erase
    
2. Функция может в некоторых ситуациях дополнятся именем существительным, которое усиливает понимание функции
    * getPlayer
    * setTitle

3. Обработчики события можно называть со слова "on" далее описание действия
    * onClick
    * onCreate
    * onRemove
    * onMove

4. Функция состоящая более чем, из двух глаголов - не корректная, т.к. он выполняет более одного действия
    * moveAndSetSpeed
    * getOrSet
    * eraseAfterDraw

5. Имя функции должно говорить в каком порядке идут параметры
    * getUserByEmailAndPassword(email, password) - Корректно
    * getUserByEmailAndPassword(password, email) - Не корректно
    * isValueIntoArray(value, array) - Корректно
    * isValueIntoArray(array, value) - Не корректно
 
6. Запрещены любые имена которые вводят в заблуждение
    * eraseOrDraw
    * getCalcSquare
    * getMinMax
    * getMinOrMax
    * gitMinAndMax

# Правила именования конструкторов/классов
1. Всегда имя начинается с символа верхнего регистра
    * User
    * Client
    * Rocket
    * Product
    * Order
    * ProductProperty
    * Property
    
2. Всегда используется имя существительное, отвечающее на вопрос что описывает класс
    * Cart
    * CartClient
    * Session
    * CustomStorage
    * BinaryTree
    * Payment

3. Недопустим использовать любой глагол
    * FinishOrder
    * CreateCartClient
    * MoveShape
    * SendEmail

4. Недопустимы имена которые вводят в заблуждение
    * RectangleCircle
    * KeyUser
    * ClientOrEmployee

5. Не используйте имя во множественном числе
    * Users
    * Orders
    * Products
    * Payments
    * Shipments
    * Clients

# Усиливание имени за счет имени объекта
1. Ослабливайте имя функции/метода и усиливайте за счет объекта
    * squareCircle - переименовываем в square
    * moveBrick - переименовываем в move
    * removeOrder - переименовываем в remove
    * addPaymentToOrder - переименовываем в addPayment
    * removeShipmentFromOrder - переименовываем в removeShipment

2. Корректное имя объекта и функции позволит понять всю суть функции/метода
    * circle.square()
    * brick.move()
    * order.remove()
    * order.addPayment(payment)
    * order.removeShipment(shipment)

# Другие правила описания функций
1. Функция выполняет одно и только одно действие
2. Если функция выполняет калькуляцию и выводит результат в html - два действия, быть такого не должно. Нужно разбить на две функции, одна калькуляцией занимается, вторая выводит в html
3. Не используйте сокращения:
    * ls - localStorage
    * hxr - XmlHttpRequest
    * ls - list
    * val - value
4. Функция всегда должна возвращать ДАННЫЕ, а не информацию
5. В коде console.log, alert, prompt и другие - не допустимы
6. Запрещено заменять return вызовом console.log
7. Не используйте параметры по умолчанию, вместо этого используйте конструкции ||, ??
8. Не используйте слишком длинные имена
    * getUsersByFirstnameAndSortByDateCreated
